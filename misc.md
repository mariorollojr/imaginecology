[AI’s Carbon Footprint Problem](https://hai.stanford.edu/blog/ais-carbon-footprint-problem),  [Deep learning has a terrible carbon footprint](https://www.technologyreview.com/2019/06/06/239031/training-a-single-ai-model-can-emit-as-much-carbon-as-five-cars-in-their-lifetimes/), [Deep Learning’s Climate Change Problem](https://www.forbes.com/sites/robtoews/2020/06/17/deep-learnings-climate-change-problem/#6f0b31396b43), [Energy and Policy Considerations for Deep Learning](https://arxiv.org/pdf/1906.02243.pdf) and [Green AI](https://arxiv.org/pdf/1907.10597.pdf)
+
[Machine Learning Emissions Calculator](https://mlco2.github.io/impact/#compute)

[WACV 2020 Animal Re-ID Workshop](https://sites.google.com/view/wacv2020animalreid/)

[Awesome Deep Ecology](https://github.com/patrickcgray/awesome-deep-ecology)
A curated list of deep learning resources for computer vision.

[Camera Trap ML Survey](https://agentmorris.github.io/camera-trap-ml-survey/)
Everything Dan Morris (Microsof) knows about machine learning and camera traps.

[Hakuna Ma-data: Identify Wildlife on the Serengeti with AI for Earth](https://github.com/drivendataorg/hakuna-madata)

[Wildlife Insight](https://wildlifeinsights.org/about)
(non free) Wildlife Insights is combining field and sensor expertise, cutting edge technology and advanced analytics to enable people everywhere to share wildlife data and better manage wildlife populations. 

[A new wave of open-source tools for analysing animal behaviour and posture](https://www.nature.com/articles/d41586-019-02942-5)

[Deep learning for videos & action recognition](http://blog.qure.ai/notes/deep-learning-for-videos-action-recognition-review)
