#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 14:04:35 2019

@author: gdussert
"""

## `Model` can be either "ResNet50" or "MobileNetV2"
Model="ResNet50"

from vis.visualization.saliency import visualize_cam
from vis.visualization import overlay
from keras.preprocessing import image
import numpy as np

if Model=="ResNet50":
     from keras.applications.resnet50 import ResNet50
     from keras.applications.resnet50 import preprocess_input, decode_predictions
     model = ResNet50(weights='imagenet') #load the model
elif Model=="MobileNetV2":
     from keras.applications.mobilenet_v2 import MobileNetV2
     from keras.applications.mobilenet_v2 import preprocess_input, decode_predictions
     model = MobileNetV2(weights='imagenet') #load the model
else:
     print(f"{Model} is not a valid model name")
     exit()

# Print all layers index and name
for i in range(len(model.layers)):
     print(i,model.layers[i],model.layers[i].name)

if Model=="ResNet50":
     dense_idx = 176 #index of last dense layer
     conv_idx = 171 #index of last convolution layer
elif Model=="MobileNetV2":
     dense_idx = 156
     conv_idx = 152

def preprocess_img(filename):
     target_size = model.input_shape[1:3] #input shape of the model
     img = image.load_img(filename, target_size=target_size) #load and resize the image
     img = image.img_to_array(img,dtype="int") #convert image to array
     x = preprocess_input(img) #preprocess the array for our model
     x = np.expand_dims(x,axis=0) # add batch dimension to call model.predict
     return img,x

def show_gradcam(filename,output_file=None,show=True):
     img,x = preprocess_img(filename) #load original and pre-processed img
     preds = model.predict(x) # model predictions
     print('Predicted:', decode_predictions(preds, top=3)[0])
     filter_indices = [np.argmax(preds[0])] #find class index of top1 prediction

     grads=visualize_cam(model, dense_idx, filter_indices, x, penultimate_layer_idx=conv_idx) #gradcam heatmap
     over=overlay(img,grads,alpha=0.6) #overlay of the original image and heatmap
     im=image.array_to_img(over) #convert array to image

     if show:
          im.show() #show image

     if output_file:
          im.save(output_file) #save image

filename = "../../images/ed-van-duijn-414NZVxzc20-unsplash.jpg"
show_gradcam(filename,output_file="gradcam_tutorial.png",show=True)
