##
## This script assumes the original images are with extension .jpg
##
## Warning: in this script, we assume that every chest is oriented with the neck on the right and the queue on the left
## As a consequence, we used LabelImg with two different labels "right" and "left" for cases where
## chests are well oriented ("right") or oriented in opposite sens("left")
## Therefore the following script search for these labels in the .xml files
## and apply two different calculus for the two cases in order to obtain the coordinates of the delineated boxes
## 
for annot in *xml; do
    name=`basename $annot .xml`
    echo $name
    WIDTH=`grep "<width>" $name.xml | cut -d ">" -f2 | cut -d "<" -f1`
    HEIGHT=`grep "<height>" $name.xml | cut -d ">" -f2 | cut -d "<" -f1`
    grep "<name>" $name.xml | cut -d ">" -f2 | cut -d "<" -f1 > /tmp/name.txt
    grep "<xmin>" $name.xml | cut -d ">" -f2 | cut -d "<" -f1 > /tmp/xmin.txt
    grep "<ymin>" $name.xml | cut -d ">" -f2 | cut -d "<" -f1 > /tmp/ymin.txt
    grep "<xmax>" $name.xml | cut -d ">" -f2 | cut -d "<" -f1 > /tmp/xmax.txt
    grep "<ymax>" $name.xml | cut -d ">" -f2 | cut -d "<" -f1 > /tmp/ymax.txt

    if [ `grep -c "left" $annot` -gt 0 ]; then
    paste /tmp/xmin.txt /tmp/xmax.txt /tmp/ymin.txt /tmp/ymax.txt /tmp/name.txt | awk -v WIDTH=$WIDTH -v HEIGHT=$HEIGHT '{
xcenter=((WIDTH-$2)+($2-$1)/2)/WIDTH;
ycenter=($3+($4-$3)/2)/HEIGHT;
xwidth=($2-$1)/WIDTH;
ywidth=($4-$3)/HEIGHT;
print 0,xcenter,ycenter,xwidth,ywidth,$5}' | grep left | head -1 | cut -f1-5 -d" " > ${name}_flop.txt
    convert ${name}.jpg -flop ${name}_flop.jpg
    fi
    if [ `grep -c "right" $annot` -gt 0 ]; then
    paste /tmp/xmin.txt /tmp/xmax.txt /tmp/ymin.txt /tmp/ymax.txt /tmp/name.txt | awk -v WIDTH=$WIDTH -v HEIGHT=$HEIGHT '{
xcenter=($1+($2-$1)/2)/WIDTH;
ycenter=($3+($4-$3)/2)/HEIGHT;
xwidth=($2-$1)/WIDTH;
ywidth=($4-$3)/HEIGHT;
print 0,xcenter,ycenter,xwidth,ywidth,$5}' | grep right | head -1 | cut -f1-5 -d" " > $name.txt
    else
    rm ${name}.jpg
    fi
done
rm *xml ###### REMARQUE le head -1 n'est surement pas utile

## FILE NOT USED
for f in *jpg; do bf=`basename $f .jpg`; if [ ! -e  $bf.txt ]; then echo $bf; fi; done
