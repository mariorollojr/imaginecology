#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 11:53:49 2020

@author: gdussert
"""

from keras.applications.xception import Xception, preprocess_input
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Dense,GlobalAveragePooling2D, Activation
from keras.models import Model
from keras.callbacks import ModelCheckpoint

## Parameters
Data_Augmentation = False

## Transfer-Learning model : we construct the classification part
base_model = Xception(include_top=False, weights="imagenet",input_shape=(299,299,3))
x = base_model.output
x = GlobalAveragePooling2D()(x)
x = Dense(512)(x) #256,1024, etc. may work as well
x = Dense(17)(x) #number of classes
preds = Activation("softmax")(x)

model = Model(inputs=base_model.input,outputs=preds)
model.compile(optimizer = "sgd", loss = 'categorical_crossentropy',metrics = ['accuracy'])

## Loading the data using generators

if Data_Augmentation:
     data_generator = ImageDataGenerator(preprocessing_function = preprocess_input,
                                        validation_split = 0.1,
                                        horizontal_flip = True,
                                        rotation_range = 10)

else:
     data_generator = ImageDataGenerator(preprocessing_function = preprocess_input,
                                        validation_split = 0.1)
batch_size = 16

train_generator = data_generator.flow_from_directory(directory = "Flowers/",
     batch_size = batch_size,
     class_mode = 'categorical',
     target_size = (299, 299),
     subset = 'training')

validation_generator = data_generator.flow_from_directory(directory = "Flowers/",
     batch_size = batch_size,
     class_mode = 'categorical',
     target_size = (299, 299),
     subset = 'validation')

## Checkpoint
weightpath = "weights" + "-{epoch:03d}.hdf5"
checkpoint = ModelCheckpoint(weightpath)

## Training
model.fit(train_generator, validation_data = validation_generator, epochs = 10, callbacks = [checkpoint])

## Prediction
from keras.preprocessing import image
import numpy as np

#classes names (found by flow_from_directory)
labels = (train_generator.class_indices)
labels = dict((v,k) for k,v in labels.items())

def predict(filename):
     img = image.load_img(filename, target_size=(299,299)) #load image
     x = image.img_to_array(img) #convert it to array
     x = np.expand_dims(x, axis=0) #simulate batch dimension
     x = preprocess_input(x) #preprocessing
     pred = model.predict(x) #classes prediction
     class_pred = labels[np.argmax(pred)] #find class with highest confidence
     conf = pred[0,np.argmax(pred)] #confidence
     print(f"Class : {class_pred} ({round(100*conf,1)}%)")


predict("test/erda-estremera-6ZyLeconAsg-unsplash.jpg")
predict("test/janice-gill-CVoa6nTXuL4-unsplash.jpg")
predict("test/jirasin-yossri-nLBw1ZMyPQg-unsplash.jpg")
predict("test/raphael-cabuis-o0jXeF8Kjkk-unsplash.jpg")
predict("test/max-conrad-9nkP0aerrN4-unsplash.jpg")
predict("test/tina-dawson-loef5xiVF0k-unsplash.jpg")
