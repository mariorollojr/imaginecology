#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 10:29:50 2019

@author: gdussert
"""

import os
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.visualize import apply_mask
from mrcnn.visualize import draw_box
from mrcnn.utils import extract_bboxes
from mrcnn.visualize import display_instances
import skimage.draw

############################################################
#  CONFIG
############################################################

class InferenceConfig(Config):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    NUM_CLASSES = 1 + 1
    IMAGES_PER_GPU = 1
    NAME="Inference"

config = InferenceConfig()
config.display()

############################################################
#  DETECT
############################################################

# Create model object in inference mode.
model = MaskRCNN(mode="inference", model_dir="", config=config)

# Load trained weights
model.load_weights("PATH_TO_WEIGHT_FILE", by_name=True)

def show_bbox_and_mask(file_path): #display the image, masks and bounding boxes
     image = skimage.io.imread(file_path)
     r = model.detect([image], verbose=1)[0]
     bbox = extract_bboxes(r['masks'])
     display_instances(image, bbox, r['masks'], r['class_ids'], ['BG', 'giraffechest'])

def save_boxes(file_path): # run the detection on an image and save the bbox prediction in the same folder
     image = skimage.io.imread(file_path)
     r = model.detect([image], verbose=1)[0]
     for i in range(r['rois'].shape[0]):
        box=r['rois'][i,:]
        image=draw_box(image,box,color=(255,0,0))
     skimage.io.imsave(file_path[:-4]+"_predictions_box.png",image)

def save_masks(file_path): # run the detection on an image and save the mask prediction in the same folder
     image = skimage.io.imread(file_path)
     r = model.detect([image], verbose=1)[0]
     for i in range(r['masks'].shape[2]):
          mask=r['masks'][:,:,i]
          image=apply_mask(image,mask,color=(1,1,1))

     skimage.io.imsave(file_path[:-4]+"_predictions_mask.png",image)

def save_boxes_folder(folder): # run the detection on all the images of a folder and save the bbox predictions in the same folder
     for file in os.listdir(folder):
          if file.endswith(".jpg") or file.endswith(".JPG") or file.endswith(".png"):
               file_path=os.path.join(folder, file)
               print(file_path)
               save_boxes(file_path)
