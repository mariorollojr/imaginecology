#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import keras

# import keras_retinanet
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color
from keras_retinanet.utils.gpu import setup_gpu

# import miscellaneous modules
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import cv2
import os
import numpy as np
import time

model_path = "resnet50_csv_08.h5" #change with absolute path to your model file

# load retinanet model
model = models.load_model(model_path, backbone_name='resnet50')

model = models.convert_model(model)


import pandas as pd
csv=pd.read_csv("class.csv",header=None) #change with absolute path to class.csv
id_to_class=list(csv[0])

def show_detection(filename): #display the detection and returns box,score,label
     # load image
     image = read_image_bgr(filename)

     # copy to draw on
     draw = image.copy()
     draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

     # preprocess image for network
     image = preprocess_image(image)
     image, scale = resize_image(image)

     # process image
     start = time.time()
     boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
     print("processing time: ", time.time() - start)

     # correct for image scale
     boxes /= scale

     # visualize detections
     for box, score, label in zip(boxes[0], scores[0], labels[0]):
         # scores are sorted so we can break
         if score < 0.5:
             break

         color = label_color(label)

         b = box.astype(int)
         draw_box(draw, b, color=color)

         caption = "{} {:.3f}".format(id_to_class[label], score)
         draw_caption(draw, b, caption)

     plt.figure(figsize=(15, 15))
     plt.axis('off')
     plt.imshow(draw)
     plt.show()
     return boxes, scores, labels
