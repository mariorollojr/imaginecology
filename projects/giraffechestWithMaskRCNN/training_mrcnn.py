#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 14:28:50 2019

@author: gdussert
"""

# Root directory of the project
PROJ_DIR = 'your_proj_dir' #absolute path to your project directory
COCO_WEIGHTS_PATH = ".../Mask_RCNN/mask_rcnn_coco.h5" # Path to trained weights file
# Import Mask RCNN
from mrcnn import utils
from mrcnn.config import Config

############################################################
#  Dataset
############################################################

# split into train and test set
from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import asarray
#from matplotlib import pyplot
from mrcnn.utils import extract_bboxes
from mrcnn.visualize import display_instances

# class that defines and loads the giraffe dataset
class GiraffechestDataset(utils.Dataset):
     # load the dataset definitions
     def load_dataset(self, dataset_dir):
          # define one class
          self.add_class("dataset", 1, "giraffechest")
          # define data locations
          images_dir = dataset_dir + '/images/'
          annotations_dir = dataset_dir + '/annots/'
          # find all images
          for filename in listdir(images_dir):
               # extract image id
               image_id = filename[:-4]
               img_path = images_dir + filename
               ann_path = annotations_dir + image_id + '.xml'
               # get the root of the document
               self.add_image('dataset', image_id=image_id, path=img_path, annotation=ann_path)

     # extract bounding boxes from an annotation file
     def extract_boxes(self, filename):
          # load and parse the file
          tree = ElementTree.parse(filename)
          # get the root of the document
          root = tree.getroot()
          # extract each bounding box
          boxes = list()
          for box in root.findall('.//bndbox'):
               xmin = int(box.find('xmin').text)
               ymin = int(box.find('ymin').text)
               xmax = int(box.find('xmax').text)
               ymax = int(box.find('ymax').text)
               coors = [xmin, ymin, xmax, ymax]
               boxes.append(coors)
          # extract image dimensions
          width = int(root.find('.//size/width').text)
          height = int(root.find('.//size/height').text)
          return boxes, width, height

     # load the masks for an image
     def load_mask(self, image_id):
          # get details of image
          info = self.image_info[image_id]
          # define box file location
          path = info['annotation']
          # load XML
          boxes, w, h = self.extract_boxes(path)
          # create one array for all masks, each on a different channel
          masks = zeros([h, w, len(boxes)], dtype='uint8')
          # create masks
          class_ids = list()
          for i in range(len(boxes)):
               box = boxes[i]
               row_s, row_e = box[1], box[3]
               col_s, col_e = box[0], box[2]
               masks[row_s:row_e, col_s:col_e, i] = 1
               class_ids.append(self.class_names.index('giraffechest'))
          return masks, asarray(class_ids, dtype='int32')

     # load an image reference
     def image_reference(self, image_id):
          info = self.image_info[image_id]
          return info['path']

# train set
train_set = GiraffechestDataset()
train_set.load_dataset(PROJ_DIR+"/dataset/train/")
train_set.prepare()
print('Train: %d' % len(train_set.image_ids))

val_set = GiraffechestDataset()
val_set.load_dataset(PROJ_DIR+"/dataset/val/")
val_set.prepare()
print('Val: %d' % len(val_set.image_ids))

## to test if dataset and mask are correctly loaded.
def show_annots(image_id):
    image = train_set.load_image(image_id)
    mask, class_ids = train_set.load_mask(image_id)
    # extract bounding boxes from the masks
    bbox = extract_bboxes(mask)
    # display image with masks and bounding boxes
    display_instances(image, bbox, mask, class_ids, train_set.class_names)

    info = train_set.image_info[image_id]#load image info
    print(info)# display on the console

for image_id in range(10): # image_id between 0 and nb_images -1.
     show_annots(image_id)


############################################################
#  CONFIG
############################################################

class GiraffechestConfig(Config):
     NAME = "Giraffechest_cfg"     # define the name of the configuration

     NUM_CLASSES = 1 + 1           # number of classes (background + giraffechest)

     STEPS_PER_EPOCH = 1500         # number of training steps per epoch

     GPU_COUNT = 1                 # number of GPU you have

     IMAGES_PER_GPU = 2            # 2 if you have a lot of VRAM on the GPU, else 1


config = GiraffechestConfig()
config.display()

############################################################
#  TRAINING
############################################################

from mrcnn.model import MaskRCNN
import imgaug

augmentation = imgaug.augmenters.Fliplr(0.5)
# define the model
model = MaskRCNN(mode='training', model_dir='./logs/', config=config)
# load weights (mscoco) and exclude the output layers
model.load_weights(COCO_WEIGHTS_PATH, by_name=True, exclude=["mrcnn_class_logits", "mrcnn_bbox_fc",  "mrcnn_bbox", "mrcnn_mask"])
# train weights (output layers or 'heads')
model.train(train_set, val_set, learning_rate=config.LEARNING_RATE, epochs=10, layers='heads',augmentation=augmentation)

