# A few tips to improve Yolo performance

We consider the generic object detection problem performed with Yolo. By "object", we mean any pattern to be found in images (e.g. giraffe chest).
More informations and specific tips can be found [here](https://github.com/AlexeyAB/darknet#how-to-improve-object-detection)

### Tip 1. Compose a training dataset with a large range of different object positions
YOLO will not detect an object if there is not an image in the training set with a similar position, size, orientation, shape, etc. So the training set should be as diverse as possible. 

For example if you just have close up photos of your object, YOLO may not find the same object on a test image if the object is further away. 

### Tip 2. Consider modifying image resulution

The image resolution of YOLO is quite small : 416x416. That means that YOLO will resize your image to this size before processing the training or the detection. After the resize some objects can become too small to be detected, to prevent that there are two possibilities : 
* After the training, increase `height` and `width` in `yolo-obj.cfg`. It improves the precision but the detection takes more time.
* Also train the model with higher resolution. It is better but takes even more time. 

**NB:** If you get the error `CUDA : out of memory` after increasing the resolution, you should increase the `subdivision` parameters `yolo-obj.cfg`.

**NB2:** `height` and `width` must always be a multiple of 32.

### Tip 3. Be carefull with object orientation

By default, YOLO randomly flips your images horizontally to artificially augment the number of images you have. However this can be a problem if the orientation of an object is important for the detection. To deactivate it, add the line `flip=0` in the [net] section of `yolo-obj.cfg` 