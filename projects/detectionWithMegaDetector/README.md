# Detection of any animal without classification

MegaDetector is a detector that can **find the position of any kind of animal** on an image,  **without predicting the species**. It is also able to detect humans and vehicles. 
In our experiments, it was succesfull with a large range of different species. However, **it is quite slow** : 7s/image on our i7 CPU and 0.5s/image on our Titan X GPU... 

MegaDetector can be usefull when :
- you already know which animal species is present on each image and you want the coordinates of the bounding boxes around any animal (to train [Yolo](../giraffechestWithYolo) for instance)
- or you want to run a classifier on only the image part where the animal is.


<center>
<img src="../../images/snake.jpg " width="600">
</center>

**NB:** The tutorial has only been tested on linux but it should also work on Windows without using the bash scripts. 

## Installation

- Python 3 and [Tensorflow](https://www.tensorflow.org/install) v1.14 (see the [GPU Guide](https://www.tensorflow.org/install/gpu) for GPU support). (see [last section]() to convert MegaDetector to Tensorflow v2.0 )

  

(newest version)

* Clone or download this repositery : [CameraTraps](https://github.com/microsoft/CameraTraps)

* The install procedure now rely on `anaconda`and is well explained in the **Installation** section on  the GitHub page. Please consider installing `anaconda` (or preferably the lightweight version `miniconda`) using your preferred Linux installer (on our `Manjaro` Linux, we used `yaourt` and got `anaconda` installed in `/opt/miniconda3/bin/conda`)

*  `anaconda` allows to create a virtual environment that is suitable to run the programs including `MegaDetector`. To activate the environment:

  ``conda activate cameratraps``

  and deactivate it (going back to your 'real' computer environment):

  ``conda deactivate cameratraps``

  NB: don't forget a ``conda init``
  
* A little trick: set the `PYTHONPATH` to the root directory, such as `export PYTHONPATH=/xxx/CameraTraps`



(old version, before December 2019)

* Clone or download this repositery : [CameraTraps](https://github.com/microsoft/CameraTraps)

* run this line from the CameraTraps directory :

  ```
  pip3 install -r requirements.txt
  ```

* Download the trained model : [megadetector_v3.pb](https://lilablobssc.blob.core.windows.net/models/camera_traps/megadetector/megadetector_v3.pb) 



#### Warning: from here, all have been tested with the old version... Please adapt the procedure to the always changing new versions...

## Detection

Download these three [images](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/megadetector/images/) and put them in an `images/` directory.

There is two different way to run the detection :

- The first one creates new images with a rectangle around the animals, useful to visualize the predictions on few images.
- The second one saves all the coordinates of the rectangles in a `json` file, useful if you want to use the coordinates for something else. 

### Detection with image output

The command to run the detection is :

```bash
python3 [PATH_1]/detection/run_tf_detector.py [PATH_2] --imageDir [PATH_3]
```

With `[PATH_1]` the path to `CameraTraps/`, `[PATH_2]`  the absolute path to `megadetector_v3.pb` and `[PATH_3]` the absolute path to `images/`

Because it makes a long and complicated command line, we proposed a script :

* Edit [run_megadetector.sh](run_megadetector.sh) and modify the variable `model_path`, `images_dir` and `CameraTraps_path`

* Run the script with :

  ```
  ./run_megadetector.sh
  ```

When it is done, all the images with the rectangles around the animals are in the same folder as `image_dir`.

Here are the [results](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/megadetector/output/) for the three images.

### Detection with json output 

* Before running the detector, add `CameraTraps` to your `PYTHONPATH` :

```
export PYTHONPATH="$PYTHONPATH:[PATH_1]"
```

with [PATH_1] the path to `CameraTraps/`

* Then you can run the detection with : 

```
python3 [PATH_1]/detection/run_tf_detector_batch.py "[PATH_2] "[PATH_3]" "[PATH_4]"
```

With `[PATH_2]`  the absolute path to `megadetector_v3.pb`, `[PATH_3]` the absolute path to `images/` and `[PATH_4]` the path and name of the output file (e.g. `output_dir/my_out_name.json`)

Once again I made a script to do it more comfortably :

* Edit [run_megadetector_batch.sh](run_megadetector_batch.sh) and modify the variable  `model_path` , `images_dir` , `output_path `and `CameraTraps_path` 

* Run the script with :

  ```
  ./run_megadetector_batch.sh
  ```

All the coordinates of the detected animals are now in the `.json` file. 

In the `.json` file, the result for one single image looks like this (see [output.json](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/megadetector/output/output.json)): 
```
"file": "path_of_an_image.jpg",
"max_detection_conf": 0.99857306,
"detections": [
{
    "category": "1",
    "conf": 0.9985730648040771,
    "bbox": [
    0.4348638355731964,
    0.17157964408397675,
    0.4094322621822357,
    0.18918316066265106
    ]
}
```

The category is 0 for "empty", 1 for "animal", 2 for "person", 3 for "group" and 4 for "vehicle".
The bounding box coordinates correspond to `[xmin, xmax, width, height]` all in relative value to the image dimension. 

A `.json` file can be easily opened in `R` or `Python` with for instance the modules `jsonlite` and `json` respectively.

For instance, it is possible to convert a `json` file into a formatted `csv` file with the `R` script  [```extract_from_json.R```](extract_from_json.R) developed by  [@oag.gimenez](https://gitlab.com/oag.gimenez)

### Converting MegaDetector to Tensorflow 2.0

The current version of MegaDetector is written for Tensorflow 1.14 and won't work if you have Tensorflow 2.0 installed. However if you don't want to downgrade Tensorflow, it is possible to convert the MegaDetector module to work with Tensorflow 2.0 with the command `tf_upgrade_v2`.

Go to the parent directory of `CameraTraps/` and run the following command : 

```
tf_upgrade_v2 --intree CameraTraps/ --outtree CameraTraps_v2/
```

Now the tutorial can be done if [PATH_1] now refers to `CameraTraps_v2/`.

`CameraTraps_path` must also be changed in `run_megadetector.sh` and `run_megadetector_batch.sh`.