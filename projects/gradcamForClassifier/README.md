# Grad-CAM: Visual Explanations from Deep Networks 



[Grad-CAM](https://arxiv.org/abs/1610.0239) is dedicated to post-treatment: it can highlight the regions of an image that were used by a model to make a prediction, for every class. It can be used to understand what the model has learned and also to check the reason why some images are misclassified. 

<center>
<img src="../../images/gradcam_zebra.png " width="224" alt="Zebra">
<img src="../../images/gradcam_mobilenet.png " width="224">
<img src="../../images/gradcam_panda.png " width="224">
</center>

The idea is to **represent on a heat-map how much the features of the last convolution layer are used in the following layers** (i.e. the fully connected part) until the final prediction. 

## Installation

* Python 3 and [Keras](https://keras.io/).

* The python package `keras-vis` : 

  ```bash
  pip install keras-vis
  ```

## How to use Grad-CAM with keras-vis

**NB :** The full code that we will detail in the following is available in [tuto_gradcam.py](tuto_gradcam.py). It is ready to run!

### Load a model

In this tutorial, we use ResNet50 and MobileNetv2 as examples but it can be done with almost any classifier. These models are in the module keras.applications and can be easily loaded with few lines of python :

```python
# For ResNet50
from keras.applications.resnet50 import ResNet50
model = ResNet50(weights='imagenet') 
```

### Indexes of last Dense and Convolution layer

The first step is to find the layer index of the last convolution layer (since this is the one that Grad-CAM will use to build the heatmap)/ In this purpose, we print all layers and look at the last ones :

```python
# Print all layers index and name
for i in range(len(model.layers)):
     print(i,model.layers[i],model.layers[i].name)
```

Here is the output :

```
...
168 <keras.layers.convolutional.Conv2D object at 0x7f5f360a7cc0> res5c_branch2b
169 <keras.layers.normalization.BatchNormalization object at 0x7f5f3604b470> bn5c_branch2b
170 <keras.layers.core.Activation object at 0x7f5f3611b1d0> activation_48
171 <keras.layers.convolutional.Conv2D object at 0x7f5f361dc240> res5c_branch2c
172 <keras.layers.normalization.BatchNormalization object at 0x7f5f362a3780> bn5c_branch2c
173 <keras.layers.merge.Add object at 0x7f5f3628d940> add_16
174 <keras.layers.core.Activation object at 0x7f5f36358ac8> activation_49
175 <keras.layers.pooling.GlobalAveragePooling2D object at 0x7f5f36358a58> avg_pool
176 <keras.layers.core.Dense object at 0x7f5f36438198> fc1000
```

The last convolution layer is layer **171**.

We must also look at the last Dense layer (which makes the class prediction), here it is layer **176**.

```python
dense_idx = 176
conv_idx = 171
```

### Pre-process an image

Let's assume we want to apply Grad-CAM on this image, freely available [here](https://images.unsplash.com/photo-1545426908-a67f44ed0291?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80) :

<center>
<img src="../../images/ed-van-duijn-414NZVxzc20-unsplash.jpg " width="400">
</center>

To use our model for a prediction, we must pre-process this image (in particular, resize the image to the dimensions expected in the first layer). Here is the corresponding code :

```python
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions

def preprocess_img(filename):
     target_size = model.input_shape[1:3] #find input shape of the model
     img = image.load_img(filename, target_size=target_size) #load and resize the image
     img = image.img_to_array(img,dtype="int") #convert image to array
     x = preprocess_input(img) #preprocess the array for our model
     x = np.expand_dims(x,axis=0) # add batch dimension to call model.predict
     return img,x
```

### Predict the class (Optional )

Now we can look at the prediction of our model. This step can be avoided if you already know the class index.

```python
img,x = preprocess_img(filename) #load original and pre-processed img
preds = model.predict(x) # model predictions
print('Predicted:', decode_predictions(preds, top=3)[0])
```

Output : 

```
Predicted: [('n02396427', 'wild_boar', 0.91659445), ('n02395406', 'hog', 0.062247213), ('n02397096', 'warthog', 0.008596109)]
```

The class `wild boar` is predicted with 91% of confidence. In ImageNet there are 1000 different classes and to use `keras-vis` we must find the class index of `wild boar`, it can be done like that :

```python
filter_indices = [np.argmax(preds[0])] #find class index of top1 prediction
```

### Compute Grad-CAM heatmap

We have everything to use the function `visualize_cam` of the package `keras-vis`, the function returns `grads` a heat-map that we can overlay on the original image. 

```python
from vis.visualization.saliency import visualize_cam
from vis.visualization import overlay

grads=visualize_cam(model, dense_idx, filter_indices, x, penultimate_layer_idx=conv_idx)
over=overlay(img,grads,alpha=0.6) #overlay of the original image and heatmap
im=image.array_to_img(over) #convert array to image
im.show() #show image
im.save("Grad-CAM.png") #save the image
```

<center>
<img src="../../images/original.png " width="224">
<img src="../../images/heatmap.png " width="224">
<img src="../../images/gradcam_resnet.png " width="224">
</center>

Here we can see that the model uses the head and the top of the boar's legs to recognize it. 

With MobileNetv2 we have a different heat-map : 

<center>
<img src="../../images/gradcam_mobilenet.png " width="224">
</center>

## How Grad-CAM works ?

In this part we explain how Grad-CAM is able to produce this heat-map. 

For ResNet50, if we use `model.summary()` we can see that the output shape of last convolution layer is `(7,7,2048)`. So there are 2048 different features, and for each feature there is a 7x7 feature map. 

The gradients between the convolution layer and final layer are then computed relatively to one class. There are 7x7x2048 gradients, and a high gradient for one element of a feature-map means that the model uses specifically this feature to determine if the image belongs to the chosen class.  These gradients are specific to the model weights and the class, they stay the same no matter which image is used. 

The image is then fed into the network but only the output of the last convolution is saved, we have 2048 7x7 feature map activations for our image. Each feature map is multiplied by the mean gradient of this feature, this makes a 7x7 heat-map for each feature.

We can now sum the 2048 heat-maps to make only one, scale it to `[0,1]` and **upscale the resolution** from `7x7` to `224x224` (the input resolution of the image) and ta-da! we have our final heatmap ! 
