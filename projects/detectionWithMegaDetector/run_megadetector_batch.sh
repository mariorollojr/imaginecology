model_path=".../megadetector_v3.pb" #absolute path to megadetector_v3.pb
images_dir=".../animal_test/" #absolute path to the directory with your images
output_path="output.json" #path and name of the output file (eg. "output_dir/my_out_name.json")
CameraTraps_path=".../CameraTraps/" #path to CameraTraps/

export PYTHONPATH="$PYTHONPATH:$CameraTraps_path"
python $CameraTraps_path/detection/run_tf_detector_batch.py "$model_path" "$images_dir" "$output_path"

