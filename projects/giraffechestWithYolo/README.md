# Detection of giraffe chests using deep transfer learning in YOLOv3

by Gaspard Dussert & Vincent Miele  ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/))

in collaboration with Bruno Spataro, Dominique Allainé, Christophe Bonenfant ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/))

[This work was presented  at Workshop GPU @CC-IN2P3 2019](https://indico.in2p3.fr/event/18772/contributions/70480/attachments/52891/68571/ccin2p3_miele030119.pdf)
<center>
<img src="../../images/giraffe_yolo.png " width="300">
</center>


**NB:** the same tutorial with the original (deprecated) Yolo code is [here](READMEv0.md)

**NB:**  this tutorial has been tested on Linux only.

### 1. Setting the training and validation set

* Annotating giraffe chests

372 images were manually annotated (i.e. chests were manually annotated) with the program [labelImg](https://github.com/tzutalin/labelImg)

`labelImg` produces `.xml` files that must be converted into `.txt` files containing one line per object (giraffe chest) in the following format:

```
[category number] [object center in X] [object center in Y] [object width in X] [object width in Y]
```

All value except `[category number]` are float values relative to width and height and must be between 0 and 1.

This can be done adapting the script [xml2txt.sh](xml2txt.sh)

Images `.jpg` and  `txt` files are available [here](http://pbil.univ-lyon1.fr/members/miele/imaginecology/giraffechest/training/), courtesy of Dominique Allainé & Christophe Bonenfant ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/)) under copyright [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

<center>
<img src="../../images/CC-BY-NC-ND.png" width="100">
</center>

* Resize the images

As you can see some images have a huge resolution (6000x4000) that will slow down the training. It is better to resize your images to a maximum resolution, for example 1024x1024. It can be done with the script [`resize.sh`](resize.sh) and the annotations will still work as they are in relative size. Replace `folder` and `dest_folder` by the complete path to `training/` and then run the script :
```
./resize.sh
```

* Separating the training and validation sets
<center>
<img src="../../images/trainvalidationtest.png" width="300">
</center>


As explained [here](https://timebutt.github.io/static/how-to-train-yolov2-to-detect-custom-objects/), separate at random the filenames in your training directory into two sets 90%/10% and write these filenames in `train.txt` and `test.txt` respectively. To do so,  it is possible to run the Python script [sampling.py](sampling.py) replacing `training/` in the Python code by the appropriate **complete** path of this directory:
```
python sampling.py
```

NB: here, "test" refers to the "validation" set.



### 2. Downloading and compiling Darknet

Official instructions are available on the [main page](https://pjreddie.com/darknet/).

Download the code on this repository, a forked project of the original code with active development and Windows support: [https://github.com/AlexeyAB/darknet](https://github.com/AlexeyAB/darknet)

Before compiling the code, modify the `Makefile` file:

* to enable the use of GPU cards with the [cuDNN library](https://developer.nvidia.com/cudnn)  which has to be previously installed on the system (requires registration at NVIDIA)
* to view images and detections using the [opencv library](https://opencv.org/) which has to be previously installed on the system
```
GPU=1
CUDNN=1
CUDNN_HALF=0
OPENCV=1
AVX=0
OPENMP=0
LIBSO=0
ZED_CAMERA=0
```

Use `CUDNN_HALF=1` if you have a GPU with Volta architecture to speed up the training even more.

When no gpu is availble, it is also possible to activate the use of `openMP` with `OPENMP=1` (in this case, the running time is O(100) longer even with many CPUs). Also use `AVX=1` if you have an Intel GPU to speed up the training. 

Compile with `make` command from the darknet folder.

### 3. Setting YOLOv3 to enable transfer learning

Since the training dataset is quite small, it is impossible to learn a model directly. The trick, aka [transfer learning](https://machinelearningmastery.com/transfer-learning-for-deep-learning/) with CNN, consists in reusing a pre-trained model as the starting point for a model on a particular task of interest (in this case, detecting giraffe chests)


<center>
<img src="../../images/transferlearning.jpeg" width="400">
</center>

* Create `cfg/` and `backup/` directories, and move the files `train.txt` and `test.txt` to `cfg/`

* Create a file `cfg/obj.names` specifying the object name to detect:
```
giraffechest
```

*  This is highly recommended to verify that the annotated files are correct using [Yolo_mark](https://github.com/AlexeyAB/Yolo_mark)
```
[path1]/Yolo_mark/build/yolo_mark [path2] cfg/train.txt cfg/obj.names
```
where `[path1]` is the directory where you have dowloaded `Yolo_mark` and `[path2]` is the directory containing `.txt` and `.jpg` files

*  As explained [here](https://medium.com/@manivannan_data/how-to-train-yolov3-to-detect-custom-objects-ccbcafeb13d2), create a configuration file [obj.data](cfg/obj.data) :
```
classes= 1  
train  = [path]/cfg/train.txt  
valid  = [path]/cfg/test.txt  
names = [path]/cfg/obj.names  
backup = [path]/backup/
```
where `[path]` is the directory containing these files.


*  Modify or copy a preexisting [yolo-obj.cfg](cfg/yolo-obj.cfg) into `cfg/`. This file contains the parameters for the training : batch size, subdivision, number of classes, etc.
 
As explained [here](https://medium.com/@manivannan_data/how-to-train-yolov3-to-detect-custom-objects-ccbcafeb13d2), this file must be adapted to the case with 1 class. Here is the `diff` between the original `darknet/cfg/yolov3.cfg` and our `yolo-obj.cfg` (`diff` is the Linux command that shows the differences between two files):
```
6,7c6,7
< batch=64
< subdivisions=16
---
> batch=24
> subdivisions=8
603c603
< filters=255
---
> filters=18
610c610
< classes=80
---
> classes=1
689c689
< filters=255
---
> filters=18
696c696
< classes=80
---
> classes=1
776c776
< filters=255
---
> filters=18
783c783
< classes=80
---
> classes=1
```

### 4. Training YOLOv3 on a GPU computer
Download the pretrained convolutional weights [darknet53.conv.74](https://pjreddie.com/media/files/darknet53.conv.74)  (i.e the pretrained model, with extension `.weight`) and move it to the main folder (the one that contain `cfg/` and `backup/` ).

**NB :** `darknet53.conv.74` weights are made only for transfer learning, if you want to do detection on a pre-trained yolov3 model use this [model](https://pjreddie.com/media/files/yolov3.weights).

It is time to run the model estimation, from the root of the `darknet` directory and [PATH1/2] being the paths to `cfg/` and `darknet53.conv.74`: 
```
export LD_LIBRARY_PATH=/usr/local/lib64/  # might be usefull to help the system to find openCV

./darknet detector train [path1]/cfg/obj.data [path1]/cfg/yolo-obj.cfg [path2]/darknet53.conv.74
```

The output of YOLO during the training is explained [here](https://timebutt.github.io/static/understanding-yolov2-training-output/). Important informations such as the number of iterations, batch loss and average loss can be found. 

The weights are saved in `yolo-obj_xxxx.weights` every 1000 iterations in the `backup` directory and the current weights are saved in `backup/yolo-obj.backup` every 100 iterations. 
One can manually save the current weights file with `cp backup/yolo-obj.backup your_file_name.weights`

When stopping the code? A the right time to prevent overfitting : when the average loss stops decreasing. More informations [here](https://github.com/AlexeyAB/darknet#when-should-i-stop-training).

<center>
<img src="../../images/overfit.png" width="300">
</center>

With OpenCV, YOLO automatically save the validation loss chart in `chart.png`. The x-axis is controlled by `max_batches` in `yolo-obj.cfg`.

Stopping after 3000 iterations should be enough (80 minutes on a NVIDIA Titan X). From here, we assume the model weights are saved into the file  `giraffechest_3000.weights`.


### 5. Detecting giraffe chests on an independent test set

If you don't have a GPU to do the training, here is a link to download the [trained model](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/giraffechest/trained_models/giraffechest_yolo.weights)
 
 It is time to run the giraffe chest detector with the previously estimated model, from the root of the `darknet` directory:
```
export LD_LIBRARY_PATH=/usr/local/lib64/  # might be usefull to help the system to find openCV

./darknet detector test [path1]/cfg/obj.data [path1]/cfg/yolo-obj.cfg [path2]/giraffechest_3000.weights [path3]/IMAGE.jpg -thresh 0.05
```
where `IMAGE.jpg` is a target image from the test set and `[path1/2/3]`are the appropriate directories. By default, YOLO only displays objects detected with a confidence of `.25` or higher. You can change this by using the `-thresh`option (here we chose a very permissive value of 0.05).

Warning: as previously explained, the [opencv library](https://opencv.org/) is required to view images and detections. If you don't have OpenCV, YOLO creates a `predictions.jpg` image in the darknet root directory.

Some images files from the test set are available [here](http://pbil.univ-lyon1.fr/members/miele/imaginecology/giraffechest/validation/), courtesy of Dominique Allainé & Christophe Bonenfant ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/))  under copyright [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

<center>
<img src="../../images/CC-BY-NC-ND.png" width="100">
</center>

### 6. Improving Yolo performance?

It is possible to improve Yolo performance. Here are [a few simple tips to take into consideration](tipsYolo.md).