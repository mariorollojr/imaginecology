model_path=".../megadetector_v3.pb" #absolute path to megadetector_v3.pb
images_dir=".../images/" #absolute path to the directory with your images
CameraTraps_path=".../CameraTraps/" #path to CameraTraps/

python3 $CameraTraps_path/detection/run_tf_detector.py "$model_path" --imageDir "$images_dir"

