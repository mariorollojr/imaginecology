# Detection of giraffe chests using deep transfer learning in Mask-RCNN

by Gaspard Dussert & Vincent Miele  ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/))

<div style="text-align: justify"> 

Mask-RCNN can perfomr object detection and image segmentation. For any object, it can predict **a bounding box** (a square containing the object) as well as **a mask** (the exact contours of the object -- see image below).
<center>
<img src="../../images/example_mask.png" width="400">
</center>

In this tutorial we will only focus on bounding boxes: our masks will actually have the same shape than the boxes in our annotations (a mask = a box). 

Since the training dataset is quite small, it is impossible to learn a model directly. The trick, aka [transfer learning](https://machinelearningmastery.com/transfer-learning-for-deep-learning/) with CNN, consists in reusing a pre-trained model as the starting point for a model on a particular task of interest (in this case, detecting giraffe chests).


<center>
<img src="../../images/transferlearning.jpeg" width="400">
</center>

In this tutorial the model has been pre-trained on the COCO dataset, the most famous one for object detection. In the dataset there are 80 object catagories, including some animals. Thanks to this, the model has already learned some general features that will help to detect any object such as giraffe chest. 

**NB:** Here is a [glossary](../../doc/README.md) of the deep learning vocabulary. 

**NB2:**  this tutorial has been tested on Linux only.


### 1. Setting the training and validation set

#### (Optional) Resizing the images and annotating the images 

This part is optional because we already prepared the giraffe dataset (see next section). However, if you want to train the model on your own dataset, you must do it. 

As an example, we go back to the original giraffe images that are available [here](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/giraffechest/images/), courtesy of Dominique Allainé & Christophe Bonenfant ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/)) under copyright [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

<center>
<img src="../../images/CC-BY-NC-ND.png" width="100">
</center>

To train the model, you to annotate the images with the coordinates of every bounding boxes around the object you are interested in (a giraffe chest, for instance). 
But as these coordinates are dependent on the image size, it is mandatory to resize your images before annotating them!!

> Why do we resize them ? 

Because when an image is too large, it will be automatically resized by Mask_RCNN during the training step every time it is used : it will encrease the computing time!

Here we are going to resize the images to a maximum size of 1024x1024. 

To do this on Linux, you can use/adapt the script [```resize.sh```](resize.sh)  (the script uses [ImageMagick](https://imagemagick.org/)) :
* Edit the script and set `folder` to your path to the images and `dest_folder` to the path where the converted images will be saved, for example `images_resized`.
* Run the script with 
```
./resize.sh
```

Now you can make the annotations, for instance with [LabelImg](https://github.com/tzutalin/labelImg) which creates `xml` files in `Pascal VOC format`. 

#### Download the images and annotations

If you did the optional step you already have a folder `images_resized` with all the resized images of giraffes, and a `annotations_xml` folder with the annotations produced with `LabelImg`. But if you don't, you can download them [here](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/giraffechest/) for the rest of the tutorial.

**NB:** Be careful, the annotations in `annotations_xml` only work for the resized images and not the original one. 

The `.xml` annotations uses the `Pascal VOC` format. The main information are in `<name>` and `<bndbox>` which contain the name of the object and the coordinates of the bounding box, such as:
```
<annotations>
     <filename>0001_flop.jpg</filename>
     <folder>images</folder>
     <size>
          <width>1024</width>
          <height>681</height>
          <depth>3</depth>         
     </size>    
     <object>
          <name>giraffechest</name>
          <pose>Unspecified</pose>
          <truncated>0</truncated>
          <difficult>0</difficult>
          <bndbox>
               <xmin>469</xmin>
               <ymin>286</ymin>
               <xmax>563</xmax>
               <ymax>361</ymax>
          </bndbox>
     </object>
</annotations>
```

#### Separating the training and validation sets

Now you need to split the dataset in two parts : training and validation. 



The script [```sampling_mrcnn.py```](sampling_mrcnn.py) does it automatically. You must provide the respective directory for the images and annotations, as well as the percentage of images in the validation part (here we choose 10%). 

We use :
```
images_dir = "images_resized/" #directory of the images
annotations_dir = "annotations_xml/" #directory of the annotations
path_data = 'dataset/' #where the images and annotations will reside now
percentage_test = 10 #percentage of images going in the validation dataset
```
then run the script:
```
python3 sampling_mrcnn.py
```

It creates the two directories ```dataset/train/```  and ```dataset/val/```, each one containing the images after the sampling and their annotations. 

### 2. Installation of Mask-RCNN

Python 3, [Tensorflow 1.14.0](https://www.tensorflow.org/install) (see the [GPU Guide](https://www.tensorflow.org/install/gpu) for GPU support) and [Keras](https://keras.io/#installation) must be installed on your machine. 

**NB:** Be careful, it won't work with Tensorflow 2.0

* Clone or download this repository : [Mask_RCNN](https://github.com/matterport/Mask_RCNN)

* From the root directory of the project (`Mask_RCNN/`), install the requirements : 

  ``` pip3 install -r requirements.txt ``` 

* And run the install procedure:

  ```python3 setup.py install``` 

Finally, download the pre-trained weights : [mask_rcnn_coco.h5](https://github.com/matterport/Mask_RCNN/releases/download/v2.0/mask_rcnn_coco.h5)

### 3. Loading the dataset in Python

To load the dataset and prepare it for Mask-RCNN, a subclass of the [Dataset](https://github.com/matterport/Mask_RCNN/blob/master/mrcnn/utils.py#L239) class must be created. This sub-class should contain at least 3 functions : **load_dataset()**, **load_mask()** and **image_reference()**.

**NB:** Mask_RCNN just need a mask because it makes the assumpion that the bounding box is the smallest rectangle that contains the mask. 

In [```training_mrcnn.py```](training_mrcnn.py) this class has already been made for our dataset and annotations in `xml` format. However it is important to understand that the functions can be rewrited to adapt to any format of annotations, especially if you want to train mask RCNN with "real" masks. 
For example we could change the function **load_dataset()** and **load_mask()** to read directly the YOLO annotations ([see other page](../giraffechestWithYolo/README.md)) without conversion to `xml` format.

That said, you can load the training and validation dataset with the following Python code : 
```
train_set = GiraffechestDataset()
train_set.load_dataset(PROJ_DIR+"/dataset/train/")
train_set.prepare()

val_set = GiraffechestDataset()
val_set.load_dataset(PROJ_DIR+"/dataset/val/")
val_set.prepare()
```
If everything worked it should display :
```
Train: 335
Val: 37
```

Finally you should check if the images and masks are correctly loaded : 
```
for image_id in range(10): # image_id between 0 and nb_images -1.
     show_annots(image_id)
```

<center>
<img src="../../images/show_annot.png" width="300">
</center>

As you can see on this image, the masks and the bounding boxes are the same and are correct. 

### 4. Configuration 

Another class must be made for the configuration of Mask_RCNN, it is a sub-class of `Config`. All the parameters that can be tuned are explained in the definition of the [Config](https://github.com/matterport/Mask_RCNN/blob/master/mrcnn/config.py) class.

**NB:** Here we assume that you are aware about the deep learning vocabulary presented in the [glossary](../../doc/README.md).

Here is the configuration we used : 
```
class GiraffechestConfig(Config):
     NAME = "Giraffechest_cfg"     # define the name of the configuration
     
     NUM_CLASSES = 1 + 1           # number of classes (background + giraffechest)
     
     STEPS_PER_EPOCH = 1500         # number of training steps per epoch
     
     GPU_COUNT = 1                 # number of GPU you have
     
     IMAGES_PER_GPU = 2            # 2 if your GPU has a lot of VRAM, else 1
```

In Mask_RCNN, ```BATCH_SIZE = GPU_COUNT * IMAGE_PER_GPU``` so here the batch size is only 2. 
With ```STEPS_PER_EPOCH``` you can control how many steps/batches are used in one epoch: here, 10 epochs correspond to a training on ```10*1500*2``` images (not "different" images; a single image is treated multiple times) .  

Because the weights are saved after each epoch, choose a reasonable number of epochs and adjust the number of steps per epoch.  


### 5. Training Mask-RCNN on a GPU computer

Starting the training is quite simple with this Python code: 

```
# augmentation parameters
augmentation = imgaug.augmenters.Fliplr(0.5)

# define the model
model = MaskRCNN(mode='training', model_dir='./logs/', config=config)

# load weights (mscoco) and exclude the output layers
model.load_weights(COCO_WEIGHTS_PATH, by_name=True, exclude=["mrcnn_class_logits", "mrcnn_bbox_fc",  "mrcnn_bbox", "mrcnn_mask"])

# train weights (output layers or 'heads')
model.train(train_set, val_set, learning_rate=config.LEARNING_RATE, epochs=20, layers='heads',augmentation=augmentation)
```
* ⚠ WARNING ⚠  We use `augmentation = imgaug.augmenters.Fliplr(0.5)` to **randomly flip the image horizontally**. This is mandatory for us because all our giraffes in the training set are oriented in the same direction (neck on the right). If you don't add this line the model won't be able to detect giraffes with the neck on the left!
* ```model_dir``` is the directory where the weights will be saved, we decide to save them in `logs/`. 
* ```COCO_WEIGHTS_PATH``` is the absolute path to ```mask_rcnn_coco.h5```.

In **model.train()**, the number of ```epochs``` can be changed and ```layers='heads'``` means that only the last layers of the model will be trained but it can also be changed. 

Starts the training like this :
```
python3 training_mrcnn.py
```

### 6. Choosing the best weights

When the training is performed, you have **a weight file for every epoch** but you should not necessary use the last one since the model can overfitted. 
To find the best weights, you must look at the validation loss graph and look at which epoch the loss is at its minimum. 

<center>
<img src="../../images/overfit.png" width="300">
</center>

To find this minimum you can either look at the output of Mask_RCNN which prints the validation loss at the end of each epoch or use [Tensorboard](https://www.tensorflow.org/tensorboard). Tensorboard is a package that comes with the Tensorflow package, very easy to use!

To use Tensorboard, use the command ```tensorboard --logdir=[PATH_1]``` where ```PATH_1``` is the absolute path to the ```model_dir``` directory you choose sooner. It will output a link like this ```http://[name_of_the_machine]:6006```, just copy/paste it in your browser to access Tensorboard.

<center>
<img src="../../images/tensorboard2.png" width="600">
</center>

**NB:** If you want to access it from an other computer on the same network, use ```http://[ip_address_of_the_machine]:6006```

**NB2:** If the command ```tensorboard``` is not found, find where it is installed with ```pip3 show tensorboard```.

The two losses we are interested in are `val_mrcnn_bbox_loss` and `val_mrcnn_class_loss`. As you can see on the screenshot, they still decrease at epoch 20, so we are going to choose this for our best weights. 


### 7. Detecting giraffe chests on an independent test set

If you don't have a GPU to do the training, here is a link to the [trained model](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/giraffechest/trained_models/giraffechest_mask_rcnn.h5).

It is time to run the giraffe chest detector with the previously estimated model, this is done in [```detect_mrcnn.py```](detect_mrcnn.py)

* Create another `Config` sub-class for inference. The only change is ```IMAGES_PER_GPU = 1``` because we will detect one image at a time.
* Create the model in inference mode 
```
model = MaskRCNN(mode="inference", model_dir=[PATH_1], config=config)
```
* Load the best weights 
```
model.load_weights([PATH_2], by_name=True)
```
with ```[PATH_2]``` the absolute path to your best weights.
* Read an image on which you want to apply detection and run the detection like this : 
```
image = skimage.io.imread(file_name)
pred = model.detect([image], verbose=1)[0]
```
* ```pred``` contains the predicted masks and bounding box coordinates. 

You can use **show_bbox_and_mask()**, **save_boxes()** and **save_boxes_folder()** defined in [```detect_mrcnn.py.py```](detect_mrcnn.py) to visualize and save the bounding boxes.

**NB:** The images doesn't need to be resized for the detection.

Some images files from the test set are available [here](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/giraffechest/test/), courtesy of Dominique Allainé & Christophe Bonenfant ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/))  under copyright [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

<center>
<img src="../../images/CC-BY-NC-ND.png" width="100">
</center>

### 8. To go further
More informations can be found in these tutorials :
- https://machinelearningmastery.com/how-to-train-an-object-detection-model-with-keras/
- https://engineering.matterport.com/splash-of-color-instance-segmentation-with-mask-r-cnn-and-tensorflow-7c761e238b46
</div>
